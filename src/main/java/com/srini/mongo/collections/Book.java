package com.srini.mongo.collections;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

/**
 * The type Book.
 */
@Data
@Builder
@Document
public class Book {

    @Id
    private String id;
    private String name;
    private String isbn;
    private String title;
    private String author;
    private BigDecimal price ;

}
