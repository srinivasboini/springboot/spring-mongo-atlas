package com.srini.mongo.controler;

import ch.qos.logback.core.testUtil.RandomUtil;
import com.srini.mongo.collections.Book;
import com.srini.mongo.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.random.RandomGenerator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The type Book controller.
 */
@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
@Slf4j
public class BookController {

    private final BookRepository bookRepository ;

    /**
     * Get all list.
     *
     * @return the list
     */
    @GetMapping("all")
    public List<Book> getAll(){
        return bookRepository.findAll() ;
    }

    /**
     * Generate books response entity.
     *
     * @param size the size
     * @return the response entity
     */
    @GetMapping("generate")
    public ResponseEntity<HttpStatus> generateBooks(@RequestParam Integer size){
        final Random random = new Random() ;
        List<Book> books = IntStream.rangeClosed(1, size)
                .mapToObj( i -> Book.builder()
                        .id(UUID.randomUUID().toString())
                        .author(String.format("Author-%s",i))
                        .isbn(String.format("ISBN-%s",i))
                        .title(String.format("TITLE-%s",i))
                        .name(String.format("NAME-%s",i))
                        .price(BigDecimal.valueOf(random.nextLong()))
                        .build())
                .peek(i -> log.info(i.toString()))
                .toList();
        bookRepository.saveAll(books) ;

        return new ResponseEntity<>(HttpStatus.ACCEPTED) ;

    }



}
