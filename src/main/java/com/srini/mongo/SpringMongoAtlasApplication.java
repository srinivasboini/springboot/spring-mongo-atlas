package com.srini.mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The type Spring mongo atlas application.
 */
@SpringBootApplication
public class SpringMongoAtlasApplication {

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringMongoAtlasApplication.class, args);
	}

}
