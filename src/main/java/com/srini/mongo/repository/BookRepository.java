package com.srini.mongo.repository;

import com.srini.mongo.collections.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * The interface Book repository.
 */
@Repository
public interface BookRepository extends MongoRepository<Book, String> {
}
